'''
File for generating data
'''
import numpy as np
import argparse
from random import seed, randint

seed(42)

def main():
    parser = argparse.ArgumentParser(description="Create input csv file.")
    parser.add_argument("--sample-size",
                        type = int,
                        help = "Number of samples to be created.",
                        default = 100)
    parser.add_argument("--interval",
                        type = int,
                        help = "Interval between time samples.",
                        default = 15)
    parser.add_argument("--seq-len",
                        type = int,
                        help = "Length of sequences.",
                        default = 10)
    parser.add_argument("--output-file",
                        type = str,
                        help = "Output file name.",
                        default = "input.csv")
    args = parser.parse_args()
    samples, interval, seqLen, outputFileName = args.sample_size, args.interval, args.seq_len, args.output_file

    data = np.zeros((samples, seqLen + 1))
    for i in range(samples):
        startingPoint = randint(0, 360)
        data[i] = np.sin(np.arange(startingPoint, (startingPoint + 1) + seqLen*interval, interval) * np.pi / 180)
    np.savetxt(outputFileName, data, fmt="%1.5f", delimiter=',')
    print("Data with {} samples saved at {}".format(samples, outputFileName))

if __name__ == "__main__":
    main()